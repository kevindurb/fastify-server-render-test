import Fastify from 'fastify';
import fastifyView from '@fastify/view';
import pug from 'pug';
import path from 'node:path';
import url from 'node:url';

const __dirname = path.dirname(url.fileURLToPath(import.meta.url));

const fastify = Fastify({
  logger: true,
});

fastify.register(fastifyView, {
  engine: { pug },
  root: path.join(__dirname, '../views'),
});

fastify.get('/', (req, reply) => {
  reply.view('index.pug', { hello: 'world' });
});

try {
  await fastify.listen({ port: 3000 });
} catch (error) {
  fastify.log.error(error);
  process.exit(1);
}
